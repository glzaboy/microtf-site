package com.microtf.framework.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * 用户信息
 *
 * @author <a href="mailto:glzaboy@163.com">glzaboy@163.com</a>
 */
@Getter
@Setter
@Entity
@Table(indexes = {
        @Index(name = "idx_object", columnList = "objectId"),
        @Index(name = "idx_traceId", columnList = "traceId")
})
public class StorageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(nullable = false)
    @Comment("主键")
    private Long id;
    /**
     * 显示名
     */
    @Column(length = 32)
    @Comment("显示名")
    String objectId;
    @Column(length = 128)
    @Comment("登录密匙")

    String url;

    Date createDate;
    /**
     * 电话
     */
    @Column(length = 64)
    String traceId;
    /**
     * N未检测，OK检测通过 fail检测未通过
     */
    @Column(length = 64)
    String checkResult;
}
