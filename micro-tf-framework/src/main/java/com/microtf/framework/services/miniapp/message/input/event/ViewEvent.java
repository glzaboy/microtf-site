package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点击菜单跳转链接时的事件推送
 * event VIEW
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ViewEvent extends Event{
    /**
     * 事件KEY值，设置的跳转URL
     */
    @JsonProperty("EventKey")
    String eventKey;
}