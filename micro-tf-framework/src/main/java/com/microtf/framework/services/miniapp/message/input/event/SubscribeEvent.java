package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 订阅
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SubscribeEvent extends Event{
    /**
     * 事件KEY值，qrscene_为前缀，后面为二维码的参数值
     */
    @JsonProperty("EventKey")
    String eventKey;
    /**
     * 二维码的ticket，可用来换取二维码图片
     */
    @JsonProperty("Ticket")
    String Ticket;
}