package com.microtf.framework.services.miniapp.message.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoMessage extends Message{
    /**
     * 消息id，64位整型
     */
    @JsonProperty(value = "MsgId")
    String msgId;
    /**
     * 视频消息媒体id，可以调用获取临时素材接口拉取数据。
     */
    @JsonProperty(value = "MediaId")
    String mediaId;
    /**
     * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    @JsonProperty(value = "ThumbMediaId")
    String thumbMediaId;
}