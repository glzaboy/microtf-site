package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class MediaCheckEvent  extends Event implements Serializable {

    @JsonProperty("appid")
    private String appid;
    /**
     * 唯一请求标识，标记单次请求
     */
    @JsonProperty("trace_id")
    private String traceId;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("errcode")
    private Integer errcode;
    @JsonProperty("errmsg")
    private String errmsg;
    @JsonProperty("result")
    private ResultDTO result;
    @JsonProperty("detail")
    private List<DetailDTO> detail;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class ResultDTO implements Serializable {
        /**
         * 建议，有risky、pass、review三种值
         */
        @JsonProperty("suggest")
        private String suggest;
        /**
         * 命中标签枚举值，100 正常；10001 广告；20001 时政；20002 色情；20003 辱骂；20006 违法犯罪；20008 欺诈；20012 低俗；20013 版权；21000 其他
         */
        @JsonProperty("label")
        private Integer label;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class DetailDTO implements Serializable {
        @JsonProperty("strategy")
        private String strategy;
        @JsonProperty("errcode")
        private Integer errcode;
        @JsonProperty("suggest")
        private String suggest;
        @JsonProperty("label")
        private Integer label;
        @JsonProperty("prob")
        private Integer prob;
    }
}
