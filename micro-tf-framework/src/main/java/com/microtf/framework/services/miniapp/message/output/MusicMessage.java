package com.microtf.framework.services.miniapp.message.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 回复视频消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MusicMessage extends OutMessage {
    public MusicMessage() {
        setMsgType("music");
    }
    @JsonProperty("Voice")
    Music music;
    @Builder
    @Data
    public static class Music{
        /**
         * 音乐标题
         */
        @JsonProperty(value = "Title")
        String title;
        /**
         * 音乐描述
         */
        @JsonProperty(value = "Description")
        String description;
        /**
         * 音乐链接
         */
        @JsonProperty(value = "MusicUrl")
        String musicUrl;
        /**
         * 高质量音乐链接，WIFI环境优先使用该链接播放音乐
         */
        @JsonProperty(value = "HQMusicUrl")
        String hQMusicUrl;
        /**
         * 缩略图的媒体id，通过素材管理中的接口上传多媒体文件，得到的id
         */
        @JsonProperty(value = "ThumbMediaId")
        String thumbMediaId;
    }
}