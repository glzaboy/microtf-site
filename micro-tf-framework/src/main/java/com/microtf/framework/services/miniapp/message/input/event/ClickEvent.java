package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义菜单事件
 * event CLICK
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ClickEvent extends Event{
    /**
     * 事件KEY值，与自定义菜单接口中KEY值对应
     */
    @JsonProperty("EventKey")
    String eventKey;
}