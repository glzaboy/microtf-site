package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 上报地理位置事件
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LocationEvent extends Event{
    /**
     * 地理位置纬度
     */
    @JsonProperty("Latitude")
    String latitude;
    /**
     * 地理位置经度
     */
    @JsonProperty("Longitude")
    String longitude;
    /**
     * 地理位置精度
     */
    @JsonProperty("Precision")
    String precision;
}