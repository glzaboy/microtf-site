package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.microtf.framework.services.miniapp.message.input.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 微信事件
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event extends Message {
    /**
     * 事件KEY值，qrscene_为前缀，后面为二维码的参数值
     */
    @JsonProperty("Event")
    String event;
}
