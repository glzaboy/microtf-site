package com.microtf.framework.services.miniapp.message.output;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 回复图文消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsMessage extends OutMessage{
    public NewsMessage() {
        setMsgType("news");
    }
    @JsonProperty("ArticleCount")
    Integer articleCount;

    @JsonProperty("item")
    List<Article> article;
    @Builder
    @Data
    public static class Article{
        /**
         * 图文消息标题
         */
        @JsonProperty(value = "Title")
        String title;
        /**
         * 图文消息描述
         */
        @JsonProperty(value = "Description")
        String description;
        /**
         * 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
         */
        @JsonProperty(value = "PicUrl")
        String picUrl;
        /**
         * 点击图文消息跳转链接
         */
        @JsonProperty(value = "Url")
        String url;
    }
}