package com.microtf.framework.services.miniapp.message.input;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {
    @JsonProperty("ToUserName")
    String toUserName;
    @JsonProperty(value = "FromUserName")
    String fromUserName;
    @JsonProperty(value = "CreateTime")
    String createTime;
    @JsonProperty(value = "MsgType")
    String msgType;
    @JsonProperty(value = "Event")
    String event;
}
