package com.microtf.framework.services.miniapp.message.input;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 小视频消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShortVideoMessage extends VideoMessage{
}