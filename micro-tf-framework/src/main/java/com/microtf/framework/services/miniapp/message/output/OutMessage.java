package com.microtf.framework.services.miniapp.message.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 回复消息
 * @author guliuzhong
 */
@Data
public class OutMessage {
    @JsonProperty("ToUserName")
    String toUserName;
    @JsonProperty(value = "FromUserName")
    String fromUserName;
    @JsonProperty(value = "CreateTime")
    String createTime;
    @JsonProperty(value = "MsgType")
    String msgType;
}