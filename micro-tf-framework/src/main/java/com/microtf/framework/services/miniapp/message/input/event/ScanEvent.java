package com.microtf.framework.services.miniapp.message.input.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 扫描带参数二维码事件
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScanEvent extends Event{
    /**
     * 事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
     */
    @JsonProperty("EventKey")
    String eventKey;
    /**
     * 二维码的ticket，可用来换取二维码图片
     */
    @JsonProperty("Ticket")
    String Ticket;
}