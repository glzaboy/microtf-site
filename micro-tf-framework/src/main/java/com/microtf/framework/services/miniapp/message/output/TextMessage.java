package com.microtf.framework.services.miniapp.message.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文本消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TextMessage extends OutMessage {
    public TextMessage() {
        this.setMsgType("text");
        this.setCreateTime(String.valueOf(System.currentTimeMillis()/1000));
    }

    /**
     * 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
     */
    @JsonProperty(value = "Content")
    String content;
}