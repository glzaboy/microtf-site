package com.microtf.framework.services.miniapp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class MessageTransfer {

    @JsonProperty("ToUserName")
    private String toUserName;
    @JsonProperty("Encrypt")
    private String encrypt;

    /**
     * 返回数据给微信服务器使用
     * 消息签名
     */
    @JsonProperty("MsgSignature")
    String msgSignature;
    /**
     * 返回数据给微信服务器使用
     * 消息时间
     */
    @JsonProperty("TimeStamp")
    String timeStamp;
    /**
     * 返回数据给微信服务器使用
     * 消息Nonce
     */
    @JsonProperty("Nonce")
    String nonce;
}
