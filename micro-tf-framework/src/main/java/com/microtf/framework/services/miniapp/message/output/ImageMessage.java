package com.microtf.framework.services.miniapp.message.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 回复图片消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ImageMessage  extends OutMessage {
    public ImageMessage() {
        setMsgType("image");
    }
    @JsonProperty("Image")
    Image image;
    @Builder
    @Data
    public static class Image{
        /**
         * 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
         */
        @JsonProperty(value = "MediaId")
        String mediaId;
    }
}
