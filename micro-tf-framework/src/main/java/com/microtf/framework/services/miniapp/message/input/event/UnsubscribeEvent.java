package com.microtf.framework.services.miniapp.message.input.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 取消订阅
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UnsubscribeEvent extends Event{
}