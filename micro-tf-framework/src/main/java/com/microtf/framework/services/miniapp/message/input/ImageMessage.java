package com.microtf.framework.services.miniapp.message.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图片消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ImageMessage extends Message{
    /**
     * 消息id，64位整型
     */
    @JsonProperty(value = "MsgId")
    String msgId;
    /**
     * 图片消息媒体id，可以调用获取临时素材接口拉取数据。
     */
    @JsonProperty(value = "MediaId")
    String mediaId;
    /**
     * 图片链接（由系统生成）
     */
    @JsonProperty(value = "PicUrl")
    String picUrl;

}