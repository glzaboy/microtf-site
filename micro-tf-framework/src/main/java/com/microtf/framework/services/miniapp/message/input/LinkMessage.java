package com.microtf.framework.services.miniapp.message.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 链接消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LinkMessage extends Message{
    /**
     * 消息id，64位整型
     */
    @JsonProperty(value = "MsgId")
    String msgId;
    /**
     * 消息标题
     */
    @JsonProperty(value = "Title")
    String title;
    /**
     * 消息描述
     */
    @JsonProperty(value = "Description")
    String description;
    /**
     * 消息链接
     */
    @JsonProperty(value = "Url")
    String url;
}