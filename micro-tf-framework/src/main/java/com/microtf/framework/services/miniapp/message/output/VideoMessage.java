package com.microtf.framework.services.miniapp.message.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 回复视频消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoMessage extends OutMessage {
    public VideoMessage() {
        setMsgType("video");
    }
    @JsonProperty("Voice")
    Video video;
    @Builder
    @Data
    public static class Video{
        /**
         * 通过素材管理中的接口上传多媒体文件，得到的id
         */
        @JsonProperty(value = "MediaId")
        String mediaId;
        /**
         * 视频消息的标题
         */
        @JsonProperty(value = "Title")
        String title;
        /**
         * 视频消息的描述
         */
        @JsonProperty(value = "Description")
        String description;
    }
}