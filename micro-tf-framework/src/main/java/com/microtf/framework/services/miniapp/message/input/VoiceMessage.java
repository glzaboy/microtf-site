package com.microtf.framework.services.miniapp.message.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 语音消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VoiceMessage extends Message{
    /**
     * 消息id，64位整型
     */
    @JsonProperty(value = "MsgId")
    String msgId;
    /**
     * 语音消息媒体id，可以调用获取临时素材接口拉取数据。
     */
    @JsonProperty(value = "MediaId")
    String mediaId;
    /**
     * 	语音格式，如amr，speex等
     */
    @JsonProperty(value = "Format")
    String format;
    /**
     * 语音识别结果，UTF8编码
     */
    @JsonProperty(value = "Recognition")
    String recognition;
}