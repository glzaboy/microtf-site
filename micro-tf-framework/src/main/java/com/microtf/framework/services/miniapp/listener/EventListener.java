package com.microtf.framework.services.miniapp.listener;

import com.microtf.framework.services.miniapp.message.input.*;
import com.microtf.framework.services.miniapp.message.input.event.*;
import com.microtf.framework.services.miniapp.message.output.OutMessage;

import java.util.Optional;

/**
 * 消息监听
 * @author guliuzhong
 */
public interface EventListener {
    /**
     * 订阅事件
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param event 订阅事件
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(SubscribeEvent event) { return Optional.empty();}

    /**
     * 取消订阅
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param event 取消订阅
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(UnsubscribeEvent event) {return Optional.empty();}

    /**
     * 上报地理位置事件
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param event 上报地理位置
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(LocationEvent event) {return Optional.empty();}

    /**
     * 扫描带参数二维码事件
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param event 扫描带参数二维码事件
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(ScanEvent event) {return Optional.empty();}

    /**
     * 点击菜单跳转链接时的事件推送
     * @param event 点击菜单跳转链接时的事件推送
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(ViewEvent event) {return Optional.empty();}
    /**
     * 音视频内容安全识别事件
     * @param event 音视频内容安全识别事件推送
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(MediaCheckEvent event) {return Optional.empty();}
    /**
     * 自定义菜单事件
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param event 自定义菜单事件
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onEvent(ClickEvent event) {return Optional.empty();}

    /**
     * 文本消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 文本消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(TextMessage message) {return Optional.empty();}

    /**
     * 图片消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 图片消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(ImageMessage message) {return Optional.empty();}

    /**
     * 链接消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 链接消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(LinkMessage message) {return Optional.empty();}

    /**
     * 地理位置消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 地理位置消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(LocationMessage message) {return Optional.empty();}

    /**
     * 小视频消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 小视频消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(ShortVideoMessage message) {return Optional.empty();}

    /**
     * 语音消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 语音消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(VoiceMessage message) {return Optional.empty();}

    /**
     * 视频消息
     * <p>返回 {@link com.microtf.framework.services.miniapp.message.output.OutMessage}或他的子类</p>
     * @param message 视频消息
     * @param <T> Message的子类
     * @return 回复微信端的消息
     */
    default <T extends OutMessage> Optional<T> onMessage(VideoMessage message) {return Optional.empty();}
}
