package com.microtf.framework.services.miniapp.message.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 回复语音消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VoiceMessage extends OutMessage {
    public VoiceMessage() {
        setMsgType("voice");
    }
    @JsonProperty("Voice")
    Voice voice;
    @Builder
    @Data
    public static class Voice{
        /**
         * 通过素材管理中的接口上传多媒体文件，得到的id
         */
        @JsonProperty(value = "MediaId")
        String mediaId;
    }
}