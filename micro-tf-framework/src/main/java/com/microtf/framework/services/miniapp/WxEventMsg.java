package com.microtf.framework.services.miniapp;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class WxEventMsg {
    private Boolean crypt;
    private String message;
}
