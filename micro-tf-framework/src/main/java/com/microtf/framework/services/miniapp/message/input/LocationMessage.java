package com.microtf.framework.services.miniapp.message.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 地理位置消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LocationMessage extends Message{
    /**
     * 消息id，64位整型
     */
    @JsonProperty(value = "MsgId")
    String msgId;
    /**
     * 地理位置纬度
     */
    @JsonProperty(value = "Location_X")
    String locationX;
    /**
     * 地理位置经度
     */
    @JsonProperty(value = "Location_Y")
    String locationY;
    /**
     * 地图缩放大小
     */
    @JsonProperty(value = "Scale")
    String scale;
    /**
     * 地理位置信息
     */
    @JsonProperty(value = "Label")
    String label;
}