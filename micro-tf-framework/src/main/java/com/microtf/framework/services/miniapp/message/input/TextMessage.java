package com.microtf.framework.services.miniapp.message.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文本消息
 * @author guliuzhong
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TextMessage extends Message{
    /**
     * 消息id，64位整型
     */
    @JsonProperty(value = "MsgId")
    String msgId;
    @JsonProperty(value = "Content")
    String content;
}