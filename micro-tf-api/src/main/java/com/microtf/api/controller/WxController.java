package com.microtf.api.controller;

import com.microtf.api.service.MiniWeChatListen;
import com.microtf.framework.services.miniapp.WxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 微信小程序接口
 *
 * @author glzaboy
 */
@RestController
@RequestMapping("wx")
@Slf4j
public class WxController {
    private WxService wxService;


    @Autowired
    public void setWxService(WxService wxService) {
        this.wxService = wxService;
    }
    private MiniWeChatListen miniWeChatListen;

    @Autowired
    public void setMiniWeChatListen(MiniWeChatListen miniWeChatListen) {
        this.miniWeChatListen = miniWeChatListen;
    }
    @RequestMapping("event/{appId}")
    @ResponseBody
    public String event(@PathVariable String appId, HttpServletRequest httpServletRequest) {
        log.info(appId);
        String s = wxService.handleMessage(appId, httpServletRequest,miniWeChatListen);
        log.info(s);
        return s;
    }

}
